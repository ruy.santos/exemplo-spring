package br.com.itau.filmes.controllers;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.filmes.models.Filme;

@RestController
public class FilmeController {

	@GetMapping("/filme")
	public ArrayList<Filme> listar() {
		ArrayList<Filme> filmes = new ArrayList<>();

		Filme filme1 = new Filme();
		filme1.setId(1);
		filme1.setTitulo("Matrix");
		filme1.setAno(1999);

		Filme filme2 = new Filme();
		filme2.setId(2);
		filme2.setTitulo("Rambo");
		filme2.setAno(1982);

		filmes.add(filme1);
		filmes.add(filme2);

		return filmes;
	}

	@GetMapping("/filme/{id}")
	public Filme buscarFilme(@PathVariable int id) {

		ArrayList<Filme> filmes = new ArrayList();

		Filme filme1 = new Filme();
		filme1.setId(1);
		filme1.setTitulo("Matrix");
		filme1.setAno(1999);

		Filme filme2 = new Filme();
		filme2.setId(2);
		filme2.setTitulo("Rambo");
		filme2.setAno(1982);

		filmes.add(filme1);
		filmes.add(filme2);

		for (Filme filme : filmes) {
			if (filme.getId() == id) {
				return filme;
			}
		}
		throw new ResponseStatusException(HttpStatus.NOT_FOUND);
	}

}
