package br.com.itau.filmes.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.filmes.models.Filme;

@RestController
public class HelloController {

	@GetMapping("/trazerfilme")
	public Filme trazerFilme() {
		// return "Esta funcionando esta bagaça !!!";

		Filme filme = new Filme();
		filme.setTitulo("Mazzaropi na terra dos Ets");
		filme.setAno(1975);
		return filme;
	}
}